INSERT INTO users (email, password, datetime_created)
VALUES
  ('johnsmith@gmail.com', 'passwordA', '2021-01-01 01:00:00'),
  ('juandelacruz@gmail.com', 'passwordB', '2021-01-01 02:00:00'),
  ('janesmith@gmail.com', 'passwordC', '2021-01-01 03:00:00'),
  ('mariadelacruz@gmail.com', 'passwordD', '2021-01-01 04:00:00'),
  ('johndoe@gmail.com', 'passwordE', '2021-01-01 05:00:00');